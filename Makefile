.PHONY: clean all build
.DEFAULT_GOAL: build
build:
	chmod +x script.sh
	make -C GIZAppv2/
	make -C mkcls-v2/
cleanall:
	make clean -C GIZAppv2/
	make clean -C mkcls-v2/
	rm -rf *.log *.classes *.snt *.cats *.gizacfg *.final *.tok.* corpus_word* *.vcb *.cooc *.timelog dict*
clean:
	rm -rf *.log *.classes *.snt *.cats *.gizacfg *.final *.tok.* corpus_word* *.vcb *.cooc *.timelog dict*
