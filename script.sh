#!/bin/bash

# In this script we assume that the target language is always meta, and the source languages those in the "for" cycle
set -e

GIZAFOLDER="./GIZAppv2/"
giza="./GIZAppv2/GIZA++"
TOOLSFOLDER="europarl/"
mkcls="./mkcls-v2/mkcls"
plain2snt=${GIZAFOLDER}"plain2snt.out"
snt2cooc=${GIZAFOLDER}"snt2cooc.out"


${TOOLSFOLDER}/tools/tokenizer.perl -l en < raw_corp.meta > corp.tok.meta

tr '[:upper:]' '[:lower:]' < corp.tok.meta > corp.tok.low.meta

${mkcls} -n10 -pcorp.tok.low.meta -Vcorp.tok.low.meta.vcb.classes

for l in "en"
do
	echo "Pre-processing: tokenizing and lowering..."

	${TOOLSFOLDER}/tools/tokenizer.perl -l ${l} < raw_corp.${l} > corp.tok.${l}

	tr '[:upper:]' '[:lower:]' < corp.tok.${l} > corp.tok.low.${l}

	echo "Finished pre-processing, starting creation of vocabulary, cooccurrence and classes..."

	${mkcls} -n10 -pcorp.tok.low.${l} -Vcorp.tok.low.${l}.vcb.classes

	$plain2snt corp.tok.low.${l} corp.tok.low.meta

	$snt2cooc corp.tok.low.${l}.vcb corp.tok.low.meta.vcb corp.tok.low.${l}_corp.tok.low.meta.snt > corp.tok.low.${l}_corp.tok.low.meta.cooc

	echo "Finished creation! Now we start, really :)"

	echo "Starting alignment: ${l} -> meta" > ${l}.timelog
	date >> ${l}.timelog

	# $giza giza.config

	echo "Finished alignment, starting merge of parts" >> ${l}.timelog

	date >> ${l}.timelog

	$giza -S corp.tok.low.${l}.vcb –T corp.tok.low.meta.vcb –C corp.tok.low.${l}_corp.tok.low.meta.snt -CoocurrenceFile corp.tok.low.${l}_corp.tok.low.meta.cooc -o dictionary

	# cat ${l}_meta.dict.A3.final > corpus_word_aligned_${l}_meta

	# rm ${l}_meta.dict.A3.final.part*

	date >> ${l}.timelog
	echo "End of process." >> ${l}.timelog
done